const request = require('request');
var num = 0;
const url = process.env.ELASTIC_HOST + "/logs/_doc/" + num;

module.exports = {

    log: (req, res, next) => {
        const data = {
            Rota: req.params[0],
            Data: new Date()
        };

        request.post(url, { json: data }, (err, resp, body) => {
            if (err)
                return next(err);

            if (resp) {
                console.log(`Log registrado com sucesso! Rota: ${req.params[0]}`);
                num++;
                next();
            }
        });
    }
};