const express = require('express');
const api = express();
const routes = require('../routes/routes');

api.use(routes);

module.exports = api;