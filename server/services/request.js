const https = require('https');

module.exports = {
    get: () => {
        return new Promise((resolve, reject) => {
            https.get(process.env.API_URL, (resp) => {
                let data = '';

                resp.on('data', (chunk) => {
                    data += chunk;
                });

                resp.on('end', () => {
                    resolve(JSON.parse(data));
                });
            })
                .on('err', (err) => {
                    reject(err);
                });
        });
    }
};