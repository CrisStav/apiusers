const request = require('../services/request');

function _ordenar(obj, comp) {
    return obj.sort((a, b) => a[comp].localeCompare(b[comp]));
}

module.exports = {

    getUsers: (req, res) => {
        const users = [];

        request.get()
            .then(function (data) {
                data.map(user => {
                    users.push({ Nome: user.name, Email: user.email, Empresa: user.company });
                });

                _ordenar(users, "Nome");
                res.status(200).json(users);
            });
    },

    getWebsites: (req, res) => {
        let sites = [];

        request.get()
            .then((data) => {
                data.map(user => {
                    sites.push(user.website);
                });
                res.status(200).send(sites);
            });
    },

    getEnderecos: (req, res) => {

        request.get()
            .then(data => {
                let ends = data.filter((user, data) => {
                    return user.address.suite.includes('Suite');
                });

                res.status(200).json(ends);
            });
    }
}; 
