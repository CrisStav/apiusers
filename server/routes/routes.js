const Router = require('express').Router();
const ApiController = require('../controllers/ApiController');
const mi = require('../config/midleware');

Router.use('*', mi.log);

Router.get('/', ApiController.getUsers);
Router.get('/websites', ApiController.getWebsites);
Router.get('/enderecos', ApiController.getEnderecos);

module.exports = Router;