const assert = require('chai').assert;
const request = require('request');
const url = 'http://localhost:8080/';

describe("Rotas", () => {
    describe("#Get", () => {
        it("Deve trazer todos os usuarios: ", (done) => {
            request.get(url, (err, resp, data) => {
                if (err) done(err);

                assert.equal(JSON.parse(resp.body).length, 10);
                done();
            });
        });
        it("Deve trazer todos os websites dos usuarios: ", (done) => {
            request.get(`${url}websites`, (err, resp, data) => {
                if (err) done(err);

                assert.equal(JSON.parse(resp.body).length, 10);
                done();
            });
        });

        it("Deve trazer os endereços dos usuários que comecem com `suite`", (done) => {
            request.get(`${url}enderecos`, (err, resp, data) => {
                if (err) done(err);

                assert.equal(JSON.parse(resp.body).length, 7);
                done();
            });
        });
    });
});