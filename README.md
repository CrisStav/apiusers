## ApiUsers

## Requisitos do sistema

 - Tenha instalado na sua máquina o Vagrant e o Virtual box
 
## Instalando as Dependencias
 
 - Instale pelo vagrant a maquina virtual do ubuntu (vagrant init generic/ubuntu1804)
 - Suba a VM com Vagrant up
 - Entre na vm via ssh com Vagrant ssh
 
 - instale o docker na VM com o comando `sudo wget -qO- https://get.docker.com/ | sh`
 - Instale o docker-compose pelo comando `curl -L https://github.com/docker/compose/releases/download/1.8.1/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose`
 - Abilite o docker-compose `chmod +x /usr/local/bin/docker-compose`
 
## Arquivos necessários
 
 - Crie o arquivo `.env` e cole o seguinte trecho:
 
>PORT=8080;  
>API_URL=https://jsonplaceholder.typicode.com/users;  
>ELASTIC_HOST=192.168.33.10:9200;  
	
- Crie o arquivo `docker-compose.yml` com o seguinte trecho:
 
>version: "2"  
>services:  
>  app:  
>   restart: always  
>   image: node:alpine  
>   volumes:  
>     - "/home/vagrant/app:/usr/src/app"  
>   ports:  
>     - "8080:8080"  
>   command: sh -c "cd /usr/src/app && npm run deploy"  
>   environment:  
>       - PORT  
>     - ELASTIC_HOST  
>     - API_URL  
>   depends_on:  
>     - elastic  
>  elastic:  
>   image: docker.elastic.co/elasticsearch/elasticsearch:6.3.2  
>   ports:  
>     - "9200:9200"
	 
## Baixar e executar a API

 - Para baixar a API execute `git clone https://CrisStav@bitbucket.org/CrisStav/apiusers.git` isso irá clonar o repositorio em sua máquina.
 - execute o comando `sysctl vm.max_map_count=262144`, isso fará com que o elasticsearch não trave na hora de subir.
 - Rode o comando `docker-compose up -d`
 
## Rotas
 
 - 192.168.33.10 - usuários (em ordem alfabetica)
 - 192.168.33.10/websites - sites (de todos os usuários)
 - 192.168.33.10/enderecos - endereÃ§os (dos usuários que contem a palavra 'suite' no endereço)

 - 192.168.33.10:9200/logs/_doc/(numero) - acesso aos logs pelo elasticsearch