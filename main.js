require('dotenv').config();
const api = require('./server/config/express');
const port = process.env.PORT;


api.listen(process.env.PORT, () => {
    console.log(`Servidor rodando na porta:${port}`);
});
